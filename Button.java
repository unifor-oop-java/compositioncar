import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;

public class Button implements ActionListener, InterfaceAddTOJPanel {

    private final JButton button;
    private final ButtonType type;

    private static final Map<ButtonType, JTextField> textFieldMap = new HashMap<>();

    // Constructor
    Button(ButtonType type) {
        this.type = type;

        this.button = new JButton(type.getButtonText());
        this.button.addActionListener(this);
        textFieldMap.put(type, new JTextField(5));
    }

    @Override
    public void addToJPanel(JPanel jPanel) {
        jPanel.add(new JLabel(type.getLabel()));
        jPanel.add(textFieldMap.get(type));
        jPanel.add(this.button);
    }
    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        JOptionPane.showMessageDialog(null, textFieldMap.get(type).getText());
    }
}
