public enum ButtonType {
    ENGINE("Engine", "Add Engine"),
    WHEELS("Wheels", "Add Wheels"),
    DOORS("Doors", "Add Doors");

    private final String label;
    private final String buttonText;

    // Constructor
    ButtonType(String label, String buttonText) {
        this.label = label;
        this.buttonText = buttonText;
    }

    public String getLabel() {
        return label;
    }

    public String getButtonText() {
        return buttonText;
    }
}