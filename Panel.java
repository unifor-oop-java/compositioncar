import javax.swing.*;

public class Panel extends JFrame {

    public Panel() {
        super("setCar");

        Button engineButton = new Button(ButtonType.ENGINE);
        Button wheelsButton = new Button(ButtonType.WHEELS);
        Button doorsButton = new Button(ButtonType.DOORS);

        JPanel jPanel = new JPanel();
        engineButton.addToJPanel(jPanel);
        wheelsButton.addToJPanel(jPanel);
        doorsButton.addToJPanel(jPanel);

        pack();
        setContentPane(jPanel);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
    }
}